
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var DURATION = 2000; // ms
var x = d3.scaleLinear().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);
var size = function(s) { return s*10; };

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

var data = [
    {id: 1,x: 1, y: 1, size: 1},
    {id: 2,x: 1, y: 4, size: 3},
    {id: 3,x: 4, y: 5, size: 1},
    {id: 4,x: 2, y: 4, size: 2}
];

var dataNew = [
    {id: 1,x: 1, y: 1, size: 1},
    {id: 5,x: 5, y: 2, size: 3},
    {id: 3,x: 1, y: 2, size: 1},
    {id: 4,x: 2, y: 4, size: 5}
];

// Set the Domain
x.domain([0, d3.max(data, function(d) { return d.x; })]);
y.domain([0, d3.max(data, function(d) { return d.y; })]);


// Add the X Axis
var xAxis = d3.axisBottom(x);
svg.append("g")
    .attr("class","x-axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

// Add the Y Axis
var yAxis = d3.axisLeft(y);
svg.append("g")
    .attr("class", "y-axis")
    .call(yAxis);

updateData(data);

function updateData(data) {

    x.domain([0, d3.max(data, function(d) { return d.x; })]);
    y.domain([0, d3.max(data, function(d) { return d.y; })]);

    // Select all circles and join data
    var circle = svg
        .selectAll("circle")
        .data(data, function(d) { return d.id; });                // UPDATE


    circle
        .style("fill", "blue")
        .transition()
        .duration(DURATION)
        .attr("cx", function (d) {
            return x(d.x);
        })
        .attr("cy", function (d) {
            return y(d.y);
        })
        .attr("r", function (d) {
            return size(d.size);
        });


    circle
        .enter()                    // ENTER
        .append("circle")
        .attr("cx", function (d) {
            return x(d.x);
        })
        .attr("cy", function (d) {
            return y(d.y);
        })
        .attr("r", function (d) {
            return size(d.size);
        })
        .style("fill", "yellow")
        .merge(circle)              // UPDATE + ENTER
        .style("stroke", "black");


    circle
        .exit()                     // EXIT
        .remove();

    // Update Axes
    svg.select(".x-axis")
        .transition()
        .duration(DURATION)
        .call(xAxis);
    svg.select(".y-axis")
        .transition()
        .duration(DURATION)
        .call(yAxis);
}

function updateWithNewData() {
    updateData(dataNew);
}